# Crear imagen basada en la imagen oficial del Nodo 6 del dockerhub
FROM node:8

# Crea un directorio donde se colocará nuestra aplicación
RUN mkdir -p /usr/src/app

# Cambiar el directorio para que nuestros comandos se ejecuten dentro de este nuevo directorio
WORKDIR /usr/src/app

# Copiar definiciones de dependencia
COPY package.json /usr/src/app

# Install dependecies
RUN npm install

# Expose the port the app runs in
COPY . /usr/src/app

# Exponer el puerto en el que se ejecuta la aplicaciónEXPOSE 3000
EXPOSE 3000

# Serve the app
CMD ["npm", "start"]

